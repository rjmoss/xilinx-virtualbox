[//]: # (Readme.md - ubuntu 18.04 host setup for virtualbox)

# Installing Virtualbox on Ubuntu 18.04 (Bionic Beaver)
Quickly install and configure virtualbox on Ubuntu 18.04 to get started building xilinx development virtual machines.

## Host Configuration

### Host Software Overview
- Ubuntu 18.04.x LTS (Bionic Beaver)
- Virtualbox 6.1.12 (or newer)

## Virtualbox Installation
- Taken from https://www.virtualbox.org/wiki/Linux_Downloads
- Extension Pack: https://www.virtualbox.org/wiki/Downloads

### Add the Virtualbox Package Source
- Create a new sources list file

```bash
bash:
$ sudo sh -c 'echo "deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian bionic contrib" > /etc/apt/sources.list.d/virtualbox.list'
$ cat /etc/apt/sources.list.d/virtualbox.list 
deb [arch=amd64] https://download.virtualbox.org/virtualbox/debian bionic contrib
```

### Download and register the Oracle public keys
- Download and register the keys
```bash
bash:
$ wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
  OK
$ wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
```

- Verify the key fingerprints from Oracle Documentation
- See: https://www.virtualbox.org/wiki/Linux_Downloads

```bash
bash:
$ sudo apt-key fingerprint oracle
  pub   rsa4096 2016-04-22 [SC]
        B9F8 D658 297A F3EF C18D  5CDF A2F6 83C5 2980 AECF
  uid           [ unknown] Oracle Corporation (VirtualBox archive signing key) <info@virtualbox.org>
  sub   rsa4096 2016-04-22 [E]

  pub   dsa1024 2010-05-18 [SC]
        7B0F AB3A 13B9 0743 5925  D9C9 5442 2A4B 98AB 5139
  uid           [ unknown] Oracle Corporation (VirtualBox archive signing key) <info@virtualbox.org>
  sub   elg2048 2010-05-18 [E]
```

### Install Virtualbox 6.1.x
- Update the package cache
```bash
bash:
$ sudo apt-get update
...
Get:11 https://download.virtualbox.org/virtualbox/debian bionic InRelease [4,428 B]                                
Get:12 https://download.virtualbox.org/virtualbox/debian bionic/contrib amd64 Packages [1,475 B]
...
```

- List the Virtualbox packages available
```bash
bash:
$ sudo apt search "virtualbox-*"
  ...
  virtualbox-5.1/unknown 5.1.38-122592~Ubuntu~bionic amd64
    Oracle VM VirtualBox

  virtualbox-5.2/unknown 5.2.44-139111~Ubuntu~bionic amd64
    Oracle VM VirtualBox

  virtualbox-6.0/unknown 6.0.24-139119~Ubuntu~bionic amd64
    Oracle VM VirtualBox

  virtualbox-6.1/unknown 6.1.28-147628~Ubuntu~bionic amd64
    Oracle VM VirtualBox
  ...
```

- Install Virtualbox
```bash
bash:
$ sudo apt-get install virtualbox-6.1
```

### Download the extension pack
- Get the latest version number
```bash
bash:
$ LatestVirtualBoxVersion=$(wget -qO - https://download.virtualbox.org/virtualbox/LATEST-STABLE.TXT)
```

- Download the latest version using wget
```bash
bash:
$ wget "https://download.virtualbox.org/virtualbox/${LatestVirtualBoxVersion}/Oracle_VM_VirtualBox_Extension_Pack-${LatestVirtualBoxVersion}.vbox-extpack"
```

### Install the extension pack
```bash
bash:
$ sudo VBoxManage extpack install --replace Oracle_VM_VirtualBox_Extension_Pack-${LatestVirtualBoxVersion}.vbox-extpack
```

### Verify extension pack is installed
```bash
bash:
$ vboxmanage list extpacks
  Extension Packs: 1
  Pack no. 0:   Oracle VM VirtualBox Extension Pack
  Version:      6.1.28
  Revision:     147628
  Edition:      
  Description:  Oracle Cloud Infrastructure integration, USB 2.0 and USB 3.0 Host Controller, Host Webcam, VirtualBox RDP, PXE ROM, Disk Encryption, NVMe.
  VRDE Module:  VBoxVRDP
  Usable:       true 
  Why unusable:
```

### Change back to the root repo folder
```bash
bash:
resources/downloads$ cd ../..
$
```