#!/bin/bash
########################################################################################
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer
#
# Created: 
#	- 11/16/2021
############################

# Ubuntu specific
BASE_OS_NAME=ubuntu
BASE_OS_VERSION=18.04.5
BASE_OS_CODENAME=bionic
BASE_OS_ARCH=amd64

# Ubuntu Revision (Xenial Xaurus) v18.04.5
# Base Tarball Image
# These are included for reference, but not used
BASE_RELEASE_URL=http://cdimage.ubuntu.com/ubuntu-base/releases/$BASE_OS_VERSION/release
BASE_RELEASE_IMAGE=$BASE_OS_NAME-base-$BASE_OS_VERSION-base-$BASE_OS_ARCH.tar.gz
BASE_RELEASE_CHECKSUM_TYPE=sha256 #sha256, ...
BASE_RELEASE_CHECKSUM_FILE=SHA256SUMS

# Server ISO Image
# For Most Current Release Version
#ISO_RELEASE_SERVER_URL=http://releases.ubuntu.com/releases/$BASE_OS_VERSION
# For Old Releases (Archived)
ISO_RELEASE_SERVER_URL=http://old-releases.ubuntu.com/releases/$BASE_OS_VERSION
ISO_RELEASE_SERVER_IMAGE=$BASE_OS_NAME-$BASE_OS_VERSION-live-server-$BASE_OS_ARCH.iso
ISO_RELEASE_SERVER_CHECKSUM_TYPE=sha256 #sha256, ...
ISO_RELEASE_SERVER_CHECKSUM_FILE=SHA256SUMS
ISO_RELEASE_SERVER_SQUASHFS_IMAGE=casper/filesystem.squashfs

# Desktop ISO Image (Alternate Definitions)
ISO_RELEASE_DESKTOP_URL=http://old-releases.ubuntu.com/releases/$BASE_OS_VERSION
ISO_RELEASE_DESKTOP_IMAGE=$BASE_OS_NAME-$BASE_OS_VERSION-desktop-$BASE_OS_ARCH.iso
ISO_RELEASE_DESKTOP_CHECKSUM_TYPE=sha256 #sha256, ...
ISO_RELEASE_DESKTOP_CHECKSUM_FILE=SHA256SUMS
ISO_RELEASE_DESKTOP_SQUASHFS_IMAGE=casper/filesystem.squashfs

# Temporary Variables for ISO image processing
ROOTFS_TMPDIR=$TMP_PATH/rootfs
SQUASHFS_TMPDIR=$TMP_PATH/unsquashfs

# Define an array of image information (for download purposes)
# [0]: Base URL
# [1]: Filename
# [2]: Checksum type
# [3]: Checksum filename

declare -A image_url
declare -A image_file
declare -A image_checksum_type
declare -A image_checksum_file
