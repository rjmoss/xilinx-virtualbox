#!/bin/bash
########################################################################################
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer, Embedded Software
#
# Created: 
#	- 8/10/2020
############################

# Ubuntu specific
BASE_OS_NAME=CentOS
BASE_OS_VERSION=8.1.1911
BASE_OS_CODENAME=
BASE_OS_ARCH=x86_64

# Downloadable ubuntu base filesystems
# 11/3/2020 - Base Root Filesystems for older ubuntu versions no longer provided
#BASE_ROOTFS_URL=http://cdimage.ubuntu.com/ubuntu-base/releases/$BASE_OS_VERSION/release
#BASE_ROOTFS_IMAGE=$BASE_OS_NAME-base-$BASE_OS_VERSION-base-$BASE_OS_ARCH.tar.gz
#BASE_ROOTFS_CHECKSUM_TYPE=sha256 #sha256
#BASE_ROOTFS_CHECKSUM_FILE=SHA256SUMS

# Downloadable ubuntu images
BASE_ISO_URL=http://vault.centos.org/$BASE_OS_VERSION/isos/$BASE_OS_ARCH
BASE_ISO_IMAGE_SERVER=$BASE_OS_NAME-$BASE_OS_VERSION-server-$BASE_OS_ARCH.iso
BASE_ISO_IMAGE_DESKTOP=$BASE_OS_NAME-$BASE_OS_VERSION-desktop-$BASE_OS_ARCH.iso
BASE_ISO_CHECKSUM_TYPE=sha256 #sha256
BASE_ISO_CHECKSUM_FILE=SHA256SUMS

# Define an array of image information (for download purposes)
# [0]: Base URL
# [1]: Filename
# [2]: Checksum type
# [3]: Checksum filename

declare -A depends_url
declare -A depends_image
declare -A depends_checksum_type
declare -A depends_checksum_file

#depends_url[0]=$BASE_ROOTFS_URL
#depends_image[0]=$BASE_ROOTFS_IMAGE
#depends_checksum_type[0]=$BASE_ROOTFS_CHECKSUM_TYPE
#depends_checksum_file[0]=$BASE_ROOTFS_CHECKSUM_FILE

depends_url[0]=$BASE_ISO_URL
depends_image[0]=$BASE_ISO_IMAGE_SERVER
depends_checksum_type[0]=$BASE_ISO_CHECKSUM_TYPE
depends_checksum_file[0]=$BASE_ISO_CHECKSUM_FILE

depends_url[1]=$BASE_ISO_URL
depends_image[1]=$BASE_ISO_IMAGE_DESKTOP
depends_checksum_type[1]=$BASE_ISO_CHECKSUM_TYPE
depends_checksum_file[1]=$BASE_ISO_CHECKSUM_FILE
