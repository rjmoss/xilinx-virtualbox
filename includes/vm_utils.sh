#!/bin/bash
########################################################################################
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer
#
# Created: 
#	- 8/10/2020
############################

# Default wait time of 5 seconds
VM_WAIT_TIME_DEFAULT=5

# Assign required variables if not already set
if [ -z ${VM_WAIT_TIME} ] || [ -n ${VM_WAIT_TIME} ]; then
	if [ $FLAG_DEBUG -ne 0 ]; then echo "No Wait Time, using default"; fi
	VM_WAIT_TIME=$VM_WAIT_TIME_DEFAULT
else
	if [ $FLAG_DEBUG -ne 0 ]; then echo "VM Wait Time Already Defined"; fi
fi

# VM State Information Utilities
#--------------------------------

get_guest_acpi_status () {
	# Sets the GUEST_STATUS variable to the PID of the ACPI Daemon if running
	GUEST_STATUS=`VBoxManage guestcontrol $XILINX_VM_NAME run "/bin/sh" --username $ROOT_ACCT --password $USER_PWD -- -c "pgrep acpid"`
}

get_guest_status () {
	# Sets the GUEST_STATUS variable
	GUEST_STATUS=`VBoxManage showvminfo $XILINX_VM_NAME --machinereadable | grep "VMState=" | cut -d '"' -f 2`
}

get_guest_state_info () {
	vboxmanage showvminfo $XILINX_VM_NAME --machinereadable | grep "VMState="
}

# VM Wait Utilities
#--------------------------------
wait_for_guest_acpid () {
	# Check guest status
	echo "--------------------------------"
	echo " Waiting for Guest ACPI Daemon"
	GUEST_STATUS=""
	while [ "$GUEST_STATUS" == "" ]; do
		get_guest_acpi_status
		if [ $FLAG_DEBUG -ne 0 ]; then printf "."; fi		
		sleep $VM_WAIT_TIME
	done
	if [ $FLAG_DEBUG -ne 0 ]; then 	printf "\n ACPI Daemon Process ($GUEST_STATUS)\n"; fi
	echo "--------------------------------"
}

wait_for_guest_running () {
	# Check guest status
	echo "--------------------------------"
	echo " Waiting for Guest (Running)"
	GUEST_STATUS=""
	while [ "$GUEST_STATUS" != "running" ]; do
		get_guest_status
		if [ $FLAG_DEBUG -ne 0 ]; then printf "."; fi		
		sleep $VM_WAIT_TIME
	done
	if [ $FLAG_DEBUG -ne 0 ]; then 	printf "\n GUEST_STATUS ($GUEST_STATUS)\n"; fi
	echo "--------------------------------"
}

wait_for_guest_poweroff () {
	# Check guest status
	echo "--------------------------------"
	echo " Waiting for Guest (Poweroff))"
	GUEST_STATUS=""
	while [ "$GUEST_STATUS" != "poweroff" ]; do
		get_guest_status
		if [ $FLAG_DEBUG -ne 0 ]; then printf "."; fi
		sleep $VM_WAIT_TIME
	done
	if [ $FLAG_DEBUG -ne 0 ]; then 	printf "\n GUEST_STATUS ($GUEST_STATUS)\n"; fi
	echo "--------------------------------"
}

wait_for_guest_ready () {
	# Attempt to run a command on the guest, if there is an error, repeat
	echo "--------------------------------"
	echo " Waiting for Guest (Exec Svc)"

	GUEST_STATUS=0
	while [ $GUEST_STATUS -eq 0 ]; do
		VBoxManage guestcontrol $XILINX_VM_NAME run \
		/bin/echo "Checking Guest Execution Status" > /dev/null 2>&1 \
		--username $ROOT_ACCT \
		--password $USER_PWD \
		2>/dev/null
		# Check Error Level for command execution status
		case $? in
			0) # Command was successful
				GUEST_STATUS=1
				;;
			*)
				GUEST_STATUS=0;
				;;
		esac
		if [ $FLAG_DEBUG -ne 0 ]; then printf "."; fi		
		sleep $VM_WAIT_TIME
	done
	if [ $FLAG_DEBUG -ne 0 ]; then printf "\n"; fi		
	echo "--------------------------------"
}

wait_for_keypress () {
	# Wait for key press
	echo "--------------------------------"
	echo " Press a key to continue..."
	echo "--------------------------------"
	read -n 1 -s
}

common_wait () {
	echo "--------------------------------"
	echo " Pausing for $VM_WAIT_TIME seconds..."
	echo "--------------------------------"
	sleep $VM_WAIT_TIME
}

# VM Generic Control Utilities
#--------------------------------
# Note: ACPIPOWERBUTTON does not work on Linux VM's in headless mode
#       So /usr/sbin/shutdown needs to be executed

# Expects several input variables to be defined, including:
# - GUEST_COMMAND   : A string holding the guest command being executed
# - ACPI_RETRIES    : The number of times to retry acpipowerbutton

# - XILINX_VM_NAME  : A string holding the name of the virtual machine
# - USER_ACCT       : A string holding the guest OS user name
# - USER_PWD		: A string holding the guest OS user password
# - ROOT_ACCT       : A string holding the guest OS root user name
#                     Note: The '_as_root' scripts expect passwordless SUDO to be configured on the target for the USER_ACCT

# 2 variants:
# execute_guest_command_shell: Executes command in separate shell session in guest OS (no local echo)
# execute_guest_command      : Executes command in current shell in guest OS (local echo)

execute_guest_command () {
	if [[ -z ${GUEST_COMMAND} ]]; then
		echo "NO COMMAND SPECIFIC"
	else
		echo "Running: ${GUEST_COMMAND}"
		VBoxManage guestcontrol $XILINX_VM_NAME run \
		 	"/bin/sh" \
		 	--username $USER_ACCT \
		 	--password $USER_PWD \
		 	-- -c \
		 	"${GUEST_COMMAND}"
	fi
}

execute_guest_command_as_root () {
	if [[ -z ${GUEST_COMMAND} ]]; then
		echo "NO COMMAND SPECIFIC"
	else
		echo "Running: ${GUEST_COMMAND} as root"
		VBoxManage guestcontrol $XILINX_VM_NAME run "/bin/sh" \
		 	--username $ROOT_ACCT \
		 	--password $USER_PWD \
		 	-- -c \
		 	"${GUEST_COMMAND}"
	fi
}

execute_guest_poweroff_acpi () {
	VBoxManage controlvm $XILINX_VM_NAME acpipowerbutton
}

execute_guest_poweroff_shutdown () {
	GUEST_COMMAND="shutdown --poweroff"
	execute_guest_command_as_root
}
