#!/bin/bash
########################################################################################
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer
#
# Created: 
#	- 8/10/2020
############################

# Set this folder as the include path
path_include="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Derive the base repo path from the include folder path (i.e. one folder up)
path_base=${path_include%/*}

# Set the downloads path
path_downloads=$path_base/resources/downloads
path_virtualbox_downloads=$path_base/resources/virtualbox