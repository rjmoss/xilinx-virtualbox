#!/bin/bash
########################################################################################
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer
#
# Created: 
#	- 11/16/2021
############################

# Virtualbox Information
VIRTUALBOX_VERSION=$(vboxmanage --version)
VIRTUALBOX_BASE_VERSION=$(vboxmanage --version | sed 's/r.*//')

# Define File system error code
EX_OSFILE=72
EX_SUCCESS=0