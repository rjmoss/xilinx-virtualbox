#!/bin/bash
########################################################################################
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer
#
# Created: 
#	- 11/16/2021
############################

# Ubuntu specific
BASE_OS_NAME=kubuntu
BASE_OS_VERSION=18.04.5
BASE_OS_CODENAME=bionic
BASE_OS_ARCH=amd64

# Desktop ISO Image (Alternate Definitions)
#ISO_RELEASE_DESKTOP_URL=http://old-releases.ubuntu.com/releases/$BASE_OS_VERSION
ISO_RELEASE_DESKTOP_URL=http://cdimage.ubuntu.com/kubuntu/releases/$BASE_OS_VERSION/release
ISO_RELEASE_DESKTOP_IMAGE=$BASE_OS_NAME-$BASE_OS_VERSION-desktop-$BASE_OS_ARCH.iso
ISO_RELEASE_DESKTOP_CHECKSUM_TYPE=sha256 #sha256, ...
ISO_RELEASE_DESKTOP_CHECKSUM_FILE=SHA256SUMS
ISO_RELEASE_DESKTOP_SQUASHFS_IMAGE=casper/filesystem.squashfs

# Temporary Variables for ISO image processing
ROOTFS_TMPDIR=$TMP_PATH/rootfs
SQUASHFS_TMPDIR=$TMP_PATH/unsquashfs

# Define an array of image information (for download purposes)
# [0]: Base URL
# [1]: Filename
# [2]: Checksum type
# [3]: Checksum filename

declare -A image_url
declare -A image_file
declare -A image_checksum_type
declare -A image_checksum_file
