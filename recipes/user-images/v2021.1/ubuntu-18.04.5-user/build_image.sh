#!/bin/bash
########################################################################################
# Virtualbox Image Build Script:
#	- Uses: Ubuntu ISO unattended install
#
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer
#
# Created: 
#	- 11/16/2021
############################

# Process Overview
# 1. Create and Configure Virtual Machine
# 2. Create 

# Get path definitions
source ../../../../includes/paths.sh
source ../../../../includes/common.sh

# Source ubuntu 18.04.5 definitions
source $path_include/ubuntu_18.04.5_defs.sh

# Source configuration information for this image build
source include/configuration.sh

# Source vm utilities
source $path_include/vm_utils.sh

# Define the guest additions
VIRTUALBOX_ADDITIONS_ISO=$path_virtualbox_downloads/VBoxGuestAdditions_${VIRTUALBOX_BASE_VERSION}.iso

# define options
function show_opts {
	echo "Syntax:"
	echo "-------"
	echo "${0} --<option>"
	echo ""
	echo "Valid Options:"
	echo "--------------"
	echo "  --debug"
	echo ""
	echo "		Enable debug output"
	echo ""
	echo "  --iso-desktop"
	echo ""
	echo "      Create virtual machine using the iso desktop release image"
	echo ""
	echo "  --iso-server"
	echo ""
	echo "      Create virtual machine using the iso server release image"
	echo ""
	echo "  --deregister-existing"
	echo ""
	echo "      If one or more virtual resources already exist (machines, disks, etc...)"
	echo "      that will conflict with a machine created by execution of this script"
	echo "      automatically deregister these resources with Virtualbox manager."
	echo "      Resources will NOT be removed from disk."
	echo ""
	echo "  --remove-existing"
	echo ""
	echo "      If one or more virtual resources already exist (machines, disks, etc...)"
	echo "      that will conflict with a machine created by execution of this script"
	echo "      automatically deregister and remove these resources from disk."
	echo ""
	echo " --rename-existing"
	echo ""
	echo "      If one or more virtual resources already exist (machines, disks, etc...)"
	echo "      that will conflict with a machine created by execution of this script"
	echo "      automatically rename these resources."
	echo ""
	echo " --help"
	echo ""
	echo "      display this syntax help"
	echo ""
}

# Init command ling argument flags
FLAG_DEBUG=0 # Enable extra debug messages
FLAG_ISO_DESKTOP_IMAGE=0 # Create virtual machine using the iso desktop release image
FLAG_ISO_SERVER_IMAGE=0 # Create virtual machine using the iso server release
FLAG_NO_INSTALL=1 # No install image supplied - flag as non install run by default
FLAG_DEREGISTER_EXISTING=0 # Deregister existing virtual machine but do not remove from disk
FLAG_REMOVE_EXISTING=0 # Remove existing virtual machine if conflicting machine resources exist on disk
FLAG_RENAME_EXISTING=0 # Rename existing virtual machine if conflicting machine resources exist on disk

# Process Command line arguments
PARAMS=""

while (("$#")); do
	case "$1" in
		--debug) # Enable debug output
			FLAG_DEBUG=1
			echo "Set: FLAG_DEBUG=$FLAG_DEBUG"
			shift
			;;
		--iso-desktop) # Create virtual machine using the iso desktop release image
			FLAG_ISO_DESKTOP_IMAGE=1
			FLAG_NO_INSTALL=0
			if [ $FLAG_DEBUG -ne 0 ]; then echo "Set: FLAG_ISO_DESKTOP_IMAGE=1"; fi
			if [ $FLAG_DEBUG -ne 0 ]; then echo "Set: FLAG_NO_INSTALL=0"; fi
			shift
			;;
		--iso-server) # Create virtual machine using the iso server release image
			FLAG_ISO_SERVER_IMAGE=1
			if [ $FLAG_DEBUG -ne 0 ]; then echo "Set: FLAG_ISO_SERVER_IMAGE=1"; fi
			if [ $FLAG_DEBUG -ne 0 ]; then echo "Set: FLAG_NO_INSTALL=0"; fi
			shift
			;;
		--deregister-existing) # deregister any existing resources with virtualbox manager
			if [ $FLAG_DEBUG -ne 0 ]; then echo "Set: FLAG_DEREGISTER_EXISTING=1,FLAG_REMOVE_EXISTING=1" ; fi
			FLAG_DEREGISTER_EXISTING=1
			FLAG_REMOVE_EXISTING=1
			shift
			;;
		--remove-existing) # remove any existing resources
			if [ $FLAG_DEBUG -ne 0 ]; then echo "Set: FLAG_REMOVE_EXISTING=1" ; fi
			FLAG_REMOVE_EXISTING=1
			shift
			;;
		--rename-existing) # rename any existing resources
			if [ $FLAG_DEBUG -ne 0 ]; then echo "Set: FLAG_RENAME_EXISTING=1" ; fi
			FLAG_RENAME_EXISTING=1
			shift
			;;
		--help) # display syntax
			show_opts
			exit 0
			;;
		-*|--*=) # unsupported flags
			echo "ERROR: Unsupported option $1" >&2
			show_opts
			exit 1
			;;
		*) # all other parameters pass through
			PARAMS="$PARAMS $1"
			shift
			;;
	esac
done

# reset positional arguments
eval set -- "$PARAMS"

########################################################################################
# Which install method? (Base, Desktop or Server)
# 
########################################################################################
# Determine installer type
if [ $FLAG_NO_INSTALL -ne 1 ]; then
	if [ $FLAG_ISO_SERVER_IMAGE -ne 0 ]; then
		if [ $FLAG_DEBUG -ne 0 ]; then echo "Ubuntu Installer (ISO SERVER)" ; fi
		UBUNTU_INSTALLER=$path_downloads/$ISO_RELEASE_SERVER_IMAGE
	elif [ $FLAG_ISO_DESKTOP_IMAGE -ne 0 ]; then
		if [ $FLAG_DEBUG -ne 0 ]; then echo "Ubuntu Installer (ISO DESKTOP)" ; fi
		UBUNTU_INSTALLER=$path_downloads/$ISO_RELEASE_DESKTOP_IMAGE
	fi
fi

# Grab Start Time
XILINX_BUILD_START_TIME=`date`

echo "-------------------------------------"
echo "Checking for conflicting resources..."
echo "-------------------------------------"
echo " - Existing Virtual Machines..."
# Check for existing virtualbox machine in virtualbox manager:
XILINX_FILE_COND=$XILINX_VM_PATH/$XILINX_VM_SETTINGS
XILINX_BUILD_COND=$(VBoxManage list vms | grep $XILINX_VM_NAME)
if [[ $XILINX_BUILD_COND != "" ]]; then
	# vm already exists, check for processing options
	echo "   - Virtualbox Machine ["$XILINX_VM_DISK_NAME"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Deregister and delete vm
		echo "   - Unregistering and deleting existing machine"
		if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage unregistervm \
		$XILINX_VM_NAME \
		--delete
		set +x # turn off command expansion
	elif [[ $FLAG_DEREGISTER_EXISTING ]]; then
		# Deregister only
		echo "   - Unregistering existing machine"
		if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage unregistervm \
		$XILINX_VM_NAME
		set +x
	else
		echo "Another Virtualbox machine with this name is already registered."
		echo "Name: " $XILINX_BUILD_COND
		echo "Please unregister, remove or rename this machine before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

echo " - Existing Virtual Machine Settings Files (unregistered)..."
if [[ -f $XILINX_FILE_COND ]]; then
	# vm settings file already exists
	echo "   - Virtualbox settings File ["$XILINX_FILE_COND"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Remove existing settings file
		echo "   - Removing existing machine settings file"
		if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi
		rm -f $XILINX_FILE_COND
		set +x
	else
		echo "A conflicting Virtualbox machine settings file was found."
		echo "Name: " $XILINX_FILE_COND
		echo "Please remove or rename this file before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

echo " - Existing Virtual Disks..."
# Check for existing virtualbox machine in virtualbox manager:
XILINX_FILE_COND=$XILINX_VM_PATH/$XILINX_VM_DISK_NAME
XILINX_BUILD_COND=$(VBoxManage list hdds | grep $XILINX_FILE_COND)
if [[ $XILINX_BUILD_COND != "" ]]; then
	# virtual disk already exists, check for processing options
	echo "   - Virtualbox Disk ["$XILINX_FILE_COND"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Deregister and delete virtual disk
		echo "   - Unregistering and deleting existing virtual disk"
		if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage closemedium \
		$XILINX_FILE_COND \
		--delete
		set +x
	elif [[ $FLAG_DEREGISTER_EXISTING ]]; then
		# Deregister only
		echo "   - Unregistering existing virtual disk"
		if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage closemedium \
		$XILINX_FILE_COND
		set +x
	else
		echo "Another Virtualbox disk with this name is already registered."
		echo "Name: " $XILINX_FILE_COND
		echo "Please close, remove or rename this virtual disk before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

echo " - Existing Virtual Disks (unregistered)..."
if [[ -f $XILINX_FILE_COND ]]; then
	# vm settings file already exists
	echo "   - Virtualbox Disk ["$XILINX_FILE_COND"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Remove existing settings file
		echo "   - Removing existing virtual disk file"
		if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi
		rm -f $XILINX_FILE_COND
		set +x
	else
		echo "A conflicting Virtualbox disk file was found."
		echo "Name: " $XILINX_FILE_COND
		echo "Please remove or rename this file before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

########################################################################################
# If not an install run, exit success
########################################################################################
if [ $FLAG_NO_INSTALL -eq 1 ]; then
	echo "*************************************"
	echo " Exiting - no installer specified"
	echo "*************************************"	
	exit $EX_SUCCESS
fi

########################################################################################
# Test for existing Virtualbox resources
########################################################################################
echo "-------------------------------------"
echo "Checking for required resources..."
echo "-------------------------------------"
echo " - Ubuntu Installer (ISO)"

# Check for the ubuntu install iso disk image
if [[ -f $UBUNTU_INSTALLER ]] || [[ -L $UBUNTU_INSTALLER ]]; then
	echo "   - OK [$UBUNTU_INSTALLER]"
else
	echo "   - MISSING [$UBUNTU_INSTALLER]"
	echo "Please copy, link or specify the correct installer location before continuing."
	echo "*************************************"
	show_opts
	echo "*************************************"
	echo "ABORT: Terminating build script."
	exit $EX_OSFILE
fi

echo " - Guest Additions ISO ..."
if [[ -f $VIRTUALBOX_ADDITIONS_ISO ]]; then
	# Guest Additions ISO already exists
	echo "	- Virtualbox Guest Additions ISO ["$VIRTUALBOX_ADDITIONS_ISO"] exists."
else
	echo "Virtualbox Guest Additions ISO was NOT found."
	echo "Location Expected: "$VIRTUALBOX_ADDITIONS_ISO
	echo "ABORT: Terminating build script."
	exit $EX_OSFILE
fi

echo "-------------------------------------"

########################################################################################
# Create new resources
########################################################################################

# Create the virtual machine
echo ""
echo "--------------------------------"
echo " Creating the Virtual Machine..."
echo " - "$XILINX_VM_DISK_NAME
echo "--------------------------------"

if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage createvm \
--name $XILINX_VM_NAME \
--ostype Ubuntu_64 \
--basefolder $XILINX_BUILD_TMPDIR \
--register

set +x

# Verify machine was created successfully
if [[ "$(VBoxManage list vms | grep $XILINX_VM_NAME)" == "" ]]; then
	# vm doesn't exist, wasn't created
	echo "Failed to create ["$XILINX_VM_NAME"]"
	exit $EX_OSFILE
fi

# Create the virtual disk
echo ""
echo "--------------------------------"
echo " Creating the Virtual Disk..."
echo " $XILINX_VM_DISK_NAME"
echo "--------------------------------"

if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage createmedium \
--filename $XILINX_VM_PATH/$XILINX_VM_DISK_NAME \
--format VDI \
--variant Standard \
--size $XILINX_VM_MAX_SIZE_MB

set +x

# Verify disk was created successfully
if [[ "$(VBoxManage showmediuminfo $XILINX_VM_PATH/$XILINX_VM_DISK_NAME | grep $XILINX_VM_PATH/$XILINX_VM_DISK_NAME)" == "" ]]; then
	# vm disk doesn't exist, wasn't created
	echo "Failed to create [$XILINX_VM_PATH/$XILINX_VM_DISK_NAME]"
	exit $EX_OSFILE
fi

# Configure virtual machine resources
echo ""
echo "--------------------------------"
echo " Configuring machine resources.."

echo " - Adding SATA controller..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storagectl $XILINX_VM_NAME \
--name SATA \
--add sata \
--controller IntelAHCI

set +x

echo "   - Attaching virtual disk image"
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storageattach $XILINX_VM_NAME \
--storagectl SATA \
--port 0 \
--device 0 \
--type hdd \
--medium $XILINX_VM_PATH/$XILINX_VM_DISK_NAME

set +x

echo " - Adding IDE controller..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storagectl $XILINX_VM_NAME \
--name IDE \
--add ide \
--controller ICH6

set +x

echo "   - Attaching installer as optical disk"
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storageattach $XILINX_VM_NAME \
--storagectl IDE \
--port 0 \
--device 0 \
--type dvddrive \
--medium $UBUNTU_INSTALLER

set +x

echo "--------------------------------"

# Configure virtual machine resources
echo ""
echo "--------------------------------"
echo " Configuring machine settings..."

echo " - Configuring virtual cpus..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--cpus $XILINX_VM_NUM_CPUS

set +x

echo " - Configuring system RAM..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--memory $XILINX_VM_MEMORY

set +x

echo " - Configuring video RAM..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--vram $XILINX_VM_VIDEO_MEMORY

set +x

echo " - Configuring extended IRQ support..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--ioapic on

set +x

echo " - Configuring device boot order..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--boot1 dvd \
--boot2 disk \
--boot3 none \
--boot4 none

set +x

echo " - Configuring audio..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--audio none

set +x

echo " - Configuring usb..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--usb on \
--usbehci on \
--usbxhci off

set +x

# Graphics Controller Options
# vboxvga  - legacy adapter with insecure 3d passthrough to gpu
# vmsvga   - default linux guest, improved performance/security over vboxvga
# vboxsvga - hybrid of vmsvga that has legacy PCI support but improved 3d support 
echo " - Configuring the graphics controller..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--graphicscontroller vmsvga

set +x

echo " - Configuring network adapters..."
if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--nic1 nat --macaddress1 $XILINX_VM_MACADDRESS \
--nic2 none \
--nic3 none \
--nic4 none

set +x

echo "--------------------------------"

# Execute unattended install
echo ""
echo "--------------------------------"
echo " Launching unattended install..."

if [ $FLAG_DEBUG -ne 0 ]; then set -x; fi

VBoxManage unattended install \
$XILINX_VM_NAME \
--user=$USER_ACCT \
--password=$USER_PWD \
--country=US \
--time-zone=EST \
--iso=$UBUNTU_INSTALLER \
--start-vm=$VM_MODE \
--install-additions \
--additions-iso=$VIRTUALBOX_ADDITIONS_ISO \
--script-template=$UBUNTU_PRESEED_TEMPLATE \
--post-install-template=$UBUNTU_POSTINSTALL_TEMPLATE 

set +x

# Wait for installation to complete
# Note sure where/if this is officially documented, but found
# https://renenyffenegger.ch/notes/Companies-Products/Oracle/VM-VirtualBox/command-line/PowerShell/unattended-os-installation

echo "--------------------------------"
echo " Waiting for unattended install completion..."

VBoxManage guestproperty wait $XILINX_VM_NAME installation_complete
VBoxManage guestproperty delete $XILINX_VM_NAME installation_complete

echo "--------------------------------"
echo " Setting default resolution to 1920x1080..."
VBoxManage setextradata $XILINX_VM_NAME "GUI/LastGuestSizeHint" "1920,1080"

# Wait for timeout to let VM completely boot
wait_for_guest_ready

# Copy Xilinx pre-install rc.local to the guest
# This is used to signal this host script the system has booted
echo "--------------------------------"
echo " Copying rc.local to guest..."
VBoxManage guestcontrol $XILINX_VM_NAME copyto \
	$XILINX_BUILD_WORKING_DIR/$UBUNTU_CONFIGS_DIR/$XILINX_PREINSTALL_TEMPLATE \
	/etc/rc.local \
	--username $ROOT_ACCT \
	--password $USER_PWD \

# Make the script executable so it run on the next power-on
GUEST_COMMAND="chmod 755 /etc/rc.local && ls -al /etc/rc.local && cat /etc/rc.local"
execute_guest_command_as_root

echo "--------------------------------"
echo " Powering off machine..."

#Note: Not using ACPI poweroff
# - Found this unreliable, even when ACPID is running on guest
#   Sometimes guest does not respond to request (or multiple requests if retried)
#wait_for_guest_acpid
#execute_guest_poweroff_acpi

GUEST_COMMAND="shutdown --poweroff"
execute_guest_command_as_root

wait_for_guest_poweroff

echo "--------------------------------"
echo " Powering on machine..."

VBoxManage startvm $XILINX_VM_NAME \
	--type $VM_MODE

echo "--------------------------------"
echo " Waiting for virtual machine to boot..."

VBoxManage guestproperty wait $XILINX_VM_NAME installation_ready
VBoxManage guestproperty delete $XILINX_VM_NAME installation_ready

wait_for_guest_ready

# Copy Xilinx dependency install script to the guest
# This is used to install all of the 
echo "--------------------------------"
echo " Copying $XILINX_DEPINSTALL_FILE to guest..."
VBoxManage guestcontrol $XILINX_VM_NAME copyto \
	$XILINX_BUILD_WORKING_DIR/$UBUNTU_CONFIGS_DIR/$XILINX_DEPINSTALL_TEMPLATE \
	/home/$USER_ACCT \
	--username $USER_ACCT \
	--password $USER_PWD \

# Make the script executable
GUEST_COMMAND="chmod a+x /home/${USER_ACCT}/${XILINX_DEPINSTALL_FILE} && ls -al /home/${USER_ACCT}/${XILINX_DEPINSTALL_FILE}"
execute_guest_command

echo "--------------------------------"
echo " Executing Xilinx Dependency Install Script..."

# Executing the installer
GUEST_COMMAND="/home/${USER_ACCT}/${XILINX_DEPINSTALL_FILE}"
execute_guest_command_as_root

# Reboot
GUEST_COMMAND="shutdown -r"
execute_guest_command_as_root

echo "--------------------------------"
echo " Waiting for Xilinx dependency install completion..."

VBoxManage guestproperty wait $XILINX_VM_NAME installation_complete

wait_for_guest_ready

echo "--------------------------------"
echo " Powering off machine..."

#Note: Not using ACPI poweroff
# - Found this unreliable, even when ACPID is running on guest
#   Sometimes guest does not respond to request (or multiple requests if retried)
#wait_for_guest_acpid
#execute_guest_poweroff_acpi

GUEST_COMMAND="shutdown --poweroff"
execute_guest_command_as_root

wait_for_guest_poweroff

echo "--------------------------------"
echo " Creating a base snapshot of the machine..."

VBoxManage snapshot $XILINX_VM_NAME take "${XILINX_VM_NAME}_base-install" \
--description="$XILINX_RELEASE_VERSION Base Tool OS Install"

# Grab End Time
XILINX_BUILD_END_TIME=`date`
# Docker Image Build Complete
echo "-----------------------------------"
echo "Task Complete..."
echo "STARTED :"$XILINX_BUILD_START_TIME
echo "ENDED   :"$XILINX_BUILD_END_TIME
echo "-----------------------------------"
