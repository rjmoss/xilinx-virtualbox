#!/bin/bash
########################################################################################
# Image Build Variable Custom Configuration:
#
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer
#
# Created: 
#	- 11/16/2021
#
########################################################################################

########################################################################################
# Xilinx Build Script Debug Tracing
########################################################################################
#FLAG_DEBUG=1 Turns shell command expansion on in Xilinx build scripts
#FLAG_DEBUG=0 Turns shell expandion off in Xilinx build scripts
FLAG_DEBUG=0

########################################################################################
# Virtual Machine Configuration Options:
########################################################################################
# Xilinx Virtual Machine Max Disk Size in MB
XILINX_VM_MAX_SIZE_MB=350000 # 350GB
XILINX_VM_NUM_CPUS=8 # 4 cores, 2 threads per core
XILINX_VM_MEMORY=16384 # 16GB RAM
XILINX_VM_VIDEO_MEMORY=16 # 16MB RAM
XILINX_VM_MACADDRESS="02deadbeef91"

########################################################################################
# Build Arguments:
########################################################################################
# User account information
# USER_ACCT: user account name within Xilinx image
# HOME_DIR : user account home directory
USER_ACCT=xilinx
USER_PWD=xilinx
ROOT_ACCT=root
HOME_DIR=/home/$USER_ACCT

# VM Start Mode
#
VM_WAIT_TIME=5
#VM_MODE="headless"
VM_MODE="gui"

# GIT Configuration
# GIT_USER_NAME: Username for git configuration
# GIT_USER_EMAIL: email address for git configuration
GIT_USER_NAME="Xilinx User"
GIT_USER_EMAIL="Xilinx.User@dummyaddress.com"

# Xilinx base OS
XILINX_BASE_OS=ubuntu
XILINX_BASE_OS_VERSION=18.04.5

# Xilinx Release Information
XILINX_RELEASE_VERSION=v2021.1

# Location the build is executed from
XILINX_BUILD_WORKING_DIR=`pwd`
XILINX_BUILD_TMPDIR=$XILINX_BUILD_WORKING_DIR/_vm

# Xilinx build image parameters
XILINX_VM_VERSION=$XLNX_RELEASE_VERSION

# Xilinx Image Name
XILINX_VM_NAME=$XILINX_RELEASE_VERSION-$XILINX_BASE_OS-$XILINX_BASE_OS_VERSION-user
XILINX_VM_PATH=$XILINX_BUILD_TMPDIR/$XILINX_VM_NAME
XILINX_VM_SETTINGS=$XILINX_VM_NAME.vbox
XILINX_VM_DISK_NAME=$XILINX_VM_NAME.vdi

# Xilinx rc.local file to signal guest is ready to run install script
XILINX_PREINSTALL_FILE="xilinx_preinstall.rc.local"
XILINX_PREINSTALL_TEMPLATE=$UBUNTU_CONFIGS_DIR/$XILINX_PREINSTALL_FILE

# Xilinx tool dependency setup script
XILINX_DEPINSTALL_FILE="xilinx_postinstall_${XILINX_RELEASE_VERSION}.sh"
XILINX_DEPINSTALL_TEMPLATE=$UBUNTU_CONFIGS_DIR/$XILINX_DEPINSTALL_FILE

########################################################################################
# Ubuntu Related Information
########################################################################################
# - Ubuntu 18.04.5 LTS Desktop ISO
#	- http://old-releases.ubuntu.com/releases/18.04.2/ubuntu-18.04-desktop-amd64.iso
#
# - Ubuntu 18.04.5 LTS Server ISO
#	- http://old-releases.ubuntu.com/releases/18.04.2/ubuntu-18.04-server-amd64.iso
#
UBUNTU_CONFIGS_DIR=configs

UBUNTU_PRESEED_FILE=ubuntu_preseed.cfg 
UBUNTU_PRESEED_TEMPLATE=$UBUNTU_CONFIGS_DIR/$UBUNTU_PRESEED_FILE

UBUNTU_POSTINSTALL_FILE=debian_postinstall.sh
UBUNTU_POSTINSTALL_TEMPLATE=$UBUNTU_CONFIGS_DIR/$UBUNTU_POSTINSTALL_FILE
