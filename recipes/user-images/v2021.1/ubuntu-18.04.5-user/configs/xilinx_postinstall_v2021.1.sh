#!/bin/bash
# This script should be run with superuser permissions
# Run as root user or execute this script with sudo
# Globals.
#
MY_LOGFILE="/var/log/xilinx_v2021-1_setup.log"
MY_EXITCODE=0

# Set Local Echo
set +x

#
# Log header.
#
echo "******************************************************************************" >> "${MY_LOGFILE}"
echo "** Xilinx v2021.1 Tool Dependency Installation" >> "${MY_LOGFILE}"
echo "** Date:    `date -R`" >> "${MY_LOGFILE}"
echo "** Started: $0 $*" >> "${MY_LOGFILE}"

# 
# 11/18/2021:Install Xilinx Tool dependencies
echo "******************************************************************************" >> "${MY_LOGFILE}"
echo "Install Xilinx Tool Dependencies" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"
# Petalinux
# add i386 architecture for zlib1g-dev dependency
dpkg --add-architecture i386 \
    && apt-get update

echo "Install Workflow Support Packages" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"

########################################################################################
# User specified packages and supporting development tools
########################################################################################
# Development tools
apt-get install -y \
    git \
    wget

# Compression support
apt-get install -y \
    tar \
    gzip \
    unzip \
    xz-utils

# Add minicom for /dev/ttyUSB* access from command line
apt-get install -y \
    minicom

echo "Install Unified Installer Dependencies" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"
########################################################################################
# Xilinx Unified Installer Dependencies:
########################################################################################
# AR76616 - Installer Hang due to missing library
# https://www.xilinx.com/support/answers/76616.html
apt-get install -y
    libtinfo5

echo "Install Petalinux Dependencies" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"
########################################################################################
# Xilinx Petalinux Dependencies:
########################################################################################
apt-get install -y \
    tofrodos \
    iproute2 \
    gawk \
    xvfb \
    git \
    make \
    net-tools \
    libncurses5 \
    libncurses5-dev

# Note: Python2 is requried, Python3 is included by default with Ubuntu 18
apt-get install -y \
    python

# Install update-inetd for tftpd \
apt-get install -y \
    update-inetd \
    tftpd \
    zlib1g-dev:i386 \
    libssl-dev \
    flex \
    bison \
    libselinux1 \
    gnupg \
    wget \
    diffstat \
    chrpath \
    socat \
    xterm \
    autoconf \
    libtool \
    libtool-bin

# Other packages
apt-get install -y \
    tar \
    unzip \
    texinfo \
    zlib1g-dev \
    gcc-multilib \
    build-essential \
    libsdl1.2-dev \
    libglib2.0-dev \
    screen \
    pax \
    gzip

# python3-gi is used by bitbake's dependency explorer
apt-get install -y \
    python3-gi

# undocumented dependencies
apt-get install -y \
    less \
    lsb-release \
    fakeroot \
    libgtk2.0-0 \
    libgtk2.0-dev

# yocto depedencies (for petalinux)
apt-get install -y \
    cpio \
    rsync \
    xorg

# Added RFSoC ZCU111 Build Support
apt-get install -y \
    idn

# Dependencies for GIT SSH support from container
apt-get install -y \
    openssh-client\
    openssh-client-ssh1

echo "Install Vivado Dependencies" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"
########################################################################################
# Xilinx Vivado Dependencies:
########################################################################################
# derived from the perl 'ldd-recursive.pl' script
apt-get install -y \
    libboost-signals-dev \
    google-perftools

# GTK is required by the Eclipse Standard Widget Toolkit (SWT)
apt-get install -y \
        default-jre

# Vitis fails to load libswt-pi4-gtk
# -----------------------------------
apt-get install -y \
        libswt-gtk-4-jni \
        libswt-gtk-4-java

# Libgtk required for Xilinx SDK
apt-get install -y \
        libgtk2.0-0

echo "Install Vitis Dependencies" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"
########################################################################################
# Xilinx Vitis Post Install installLibs.sh script:
########################################################################################
# AIE Tools
apt-get install -y \
    libc6-dev-i386 \
    net-tools \
    graphviz

# Vitis
apt-get install -y \
    unzip \
    g++ \
    libtinfo5

########################################################################################
# Vitis AI Repository Script dependency
########################################################################################
# Partion tools for creating SD Card images
apt-get install -y \
    parted

########################################################################################
# Xilinx Vitis Dependencies:
########################################################################################
# undocumented vitis dependencies
# ----------------------------
# Failure to install QEMU data
# Setting it up...Failed to replace perl. Relocate script failed. Abort!, stderr:
# xargs: file: No such file or directory
# ----------------------------
# Requires 'file' to be installed
apt-get install -y \
    file

# petalinux-gblibc-x86_64-petalinux-image-minimal-aarch64-toolchain.sh requires this in post install
apt-get install -y \
        gcc

# Undocumented Vivado Dependency for running cmake
# Uncovered during OpenAMP Pre-built Example Generation
apt-get install -y \
    libidn11

########################################################################################
# Vitis OpenCL Installable Client Driver Loader dependencies
########################################################################################
apt-get install -y \
    ocl-icd-libopencl1 \
    opencl-headers \
    ocl-icd-opencl-dev

# 
# 11/18/2021:Cleanup apt cache and temporary files
echo "******************************************************************************" >> "${MY_LOGFILE}"
echo "Cleaning up APT Cache and temporary filess" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"
apt-get clean \
    && rm -rf "/var/lib/apt/lsts/*" "/tmp/*" "/var/tmp/*"

#
# 11/18/2021: Signal Virtualbox that installation is complete
# - Create /etc/rc.local, which will run at startup
# - Signal host OS using VBoxControl to set a guest property in the VM
# - Delete self so it only runs once at startup
echo "******************************************************************************" >> "${MY_LOGFILE}"
echo "Setting up /etc/rc.local to runonce" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"

printf '#!/bin/bash -e\nVBoxControl guestproperty set installation_complete y\n' > "/etc/rc.local"
printf 'echo done > /var/log/vboxinstall.complete\n' >> "/etc/rc.local"
printf '#Delete Self\nrm $0' >> "/etc/rc.local"
chmod 755 "/etc/rc.local"

echo "******************************************************************************" >> "${MY_LOGFILE}"
cat "/etc/rc.local" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"

#
# Add cleanup 
#
# Log footer.
#
echo "******************************************************************************" >> "${MY_LOGFILE}"
echo "** Date:            `date -R`" >> "${MY_LOGFILE}"
echo "** Final exit code: ${MY_EXITCODE}" >> "${MY_LOGFILE}"
echo "******************************************************************************" >> "${MY_LOGFILE}"

# Unset Local Echo
set -x

exit ${MY_EXITCODE}
