#!/bin/bash
########################################################################################
# Virtualbox Image Build Script:
#	- Uses: Ubuntu ISO unattended install
#
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer, Embedded Software
#
# Created: 
#	- 8/5/2020
############################

# Source configuration information for a v2020.1 Base Image build
source include/configuration.sh

# Get path definitions
source ../../../../includes/paths.sh

# Define the guest additions
VIRTUALBOX_ADDITIONS_ISO=$(ls $path_downloads/VBoxGuestAdditions*.iso)

# Define post-install command to set flag using VBoxControl
POSTINSTALL_COMMAND=\
'printf "#!/bin/bash -e\nVBoxControl guestproperty set installation_complete y\n" > /etc/rc.local'\
' && chmod +x /etc/rc.local'


# define options
function show_opts {
	echo "Syntax:"
	echo "-------"
	echo "${0} --<option>"
	echo ""
	echo "Valid Options:"
	echo "--------------"
	echo "  --debug"
	echo ""
	echo "		Enable debug output"
	echo ""
	echo "  --deregister-existing"
	echo ""
	echo "      If one or more virtual resources already exist (machines, disks, etc...)"
	echo "      that will conflict with a machine created by execution of this script"
	echo "      automatically deregister these resources with Virtualbox manager."
	echo "      Resources will NOT be removed from disk."
	echo ""
	echo "  --ubuntu-installer=<installer_path>"
	echo ""
	echo "      Specify the location of the Ubuntu ISO installer image"
	echo ""
	echo "  --remove-existing"
	echo ""
	echo "      If one or more virtual resources already exist (machines, disks, etc...)"
	echo "      that will conflict with a machine created by execution of this script"
	echo "      automatically deregister and remove these resources from disk."
	echo ""
	echo " --rename-existing"
	echo ""
	echo "      If one or more virtual resources already exist (machines, disks, etc...)"
	echo "      that will conflict with a machine created by execution of this script"
	echo "      automatically rename these resources."
	echo ""
	echo " --help"
	echo ""
	echo "      display this syntax help"
	echo ""
}

# Process Command line arguments
PARAMS=""

while (("$#")); do
	case "$1" in
		--debug) # Enable debug output
			BUILD_DEBUG=1
			echo "Set: BUILD_DEBUG=$BUILD_DEBUG"
			shift
			;;
		--deregister-existing) # deregister any existing resources with virtualbox manager
			if [ $BUILD_DEBUG -ne 0 ]; then echo "Set: FLAG_DEREGISTER_EXISTING=1,FLAG_REMOVE_EXISTING=1" ; fi
			FLAG_DEREGISTER_EXISTING=1
			FLAG_REMOVE_EXISTING=1
			shift
			;;
		--remove-existing) # remove any existing resources
			if [ $BUILD_DEBUG -ne 0 ]; then echo "Set: FLAG_REMOVE_EXISTING=1" ; fi
			FLAG_REMOVE_EXISTING=1
			shift
			;;
		--rename-existing) # remove any existing resources
			if [ $BUILD_DEBUG -ne 0 ]; then echo "Set: FLAG_RENAME_EXISTING=1" ; fi
			FLAG_RENAME_EXISTING=1
			shift
			;;
		--ubuntu-installer=*) # specify the full ubuntu installer path
			if [ $BUILD_DEBUG -ne 0 ]; then echo "Set: FLAG_UBUNTU_INSTALLER=1" ; fi
			if [ $BUILD_DEBUG -ne 0 ]; then echo "Set: UBUNTU_INSTALLER=${1#*=}" ; fi
			FLAG_UBUNTU_INSTALLER=1
			UBUNTU_INSTALLER="${1#*=}"
			shift
			;;
		--help) # display syntax
			show_opts
			exit 0
			;;
		-*|--*=) # unsupported flags
			echo "ERROR: Unsupported option $1" >&2
			show_opts
			exit 1
			;;
		*) # all other parameters pass through
			PARAMS="$PARAMS $1"
			shift
			;;
	esac
done

# reset positional arguments
eval set -- "$PARAMS"

# Grab Start Time
XILINX_BUILD_START_TIME=`date`

########################################################################################
# Test for existing Virtualbox resources
########################################################################################
echo "-------------------------------------"
echo "Checking for required resources..."
echo "-------------------------------------"
echo " - Ubuntu Installer (ISO)"

# Check for the ubuntu install iso disk image
if [[ -f $UBUNTU_INSTALLER ]] || [[ -L $UBUNTU_INSTALLER ]]; then
	echo "   - OK [$UBUNTU_INSTALLER]"
else
	echo "   - MISSING [$UBUNTU_INSTALLER]"
	echo "Please copy, link or specify the correct installer location before continuing."
	echo "*************************************"
	show_opts
	echo "*************************************"
	echo "ABORT: Terminating build script."
	exit $EX_OSFILE
fi

echo "-------------------------------------"
echo "Checking for conflicting resources..."
echo "-------------------------------------"
echo " - Existing Virtual Machines..."
# Check for existing virtualbox machine in virtualbox manager:
XILINX_FILE_COND=$XILINX_VM_PATH/$XILINX_VM_SETTINGS
XILINX_BUILD_COND=$(VBoxManage list vms | grep $XILINX_VM_NAME)
if [[ $XILINX_BUILD_COND != "" ]]; then
	# vm already exists, check for processing options
	echo "   - Virtualbox Machine ["$XILINX_VM_DISK_NAME"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Deregister and delete vm
		echo "   - Unregistering and deleting existing machine"
		if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage unregistervm \
		$XILINX_VM_NAME \
		--delete
		set +x # turn off command expansion
	elif [[ $FLAG_DEREGISTER_EXISTING ]]; then
		# Deregister only
		echo "   - Unregistering existing machine"
		if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage unregistervm \
		$XILINX_VM_NAME
		set +x
	else
		echo "Another Virtualbox machine with this name is already registered."
		echo "Name: " $XILINX_BUILD_COND
		echo "Please unregister, remove or rename this machine before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

echo " - Existing Virtual Machine Settings Files (unregistered)..."
if [[ -f $XILINX_FILE_COND ]]; then
	# vm settings file already exists
	echo "   - Virtualbox settings File ["$XILINX_FILE_COND"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Remove existing settings file
		echo "   - Removing existing machine settings file"
		if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi
		rm -f $XILINX_FILE_COND
		set +x
	else
		echo "A conflicting Virtualbox machine settings file was found."
		echo "Name: " $XILINX_FILE_COND
		echo "Please remove or rename this file before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

echo " - Existing Virtual Disks..."
# Check for existing virtualbox machine in virtualbox manager:
XILINX_FILE_COND=$XILINX_VM_PATH/$XILINX_VM_DISK_NAME
XILINX_BUILD_COND=$(VBoxManage list hdds | grep $XILINX_FILE_COND)
if [[ $XILINX_BUILD_COND != "" ]]; then
	# virtual disk already exists, check for processing options
	echo "   - Virtualbox Disk ["$XILINX_FILE_COND"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Deregister and delete virtual disk
		echo "   - Unregistering and deleting existing virtual disk"
		if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage closemedium \
		$XILINX_FILE_COND \
		--delete
		set +x
	elif [[ $FLAG_DEREGISTER_EXISTING ]]; then
		# Deregister only
		echo "   - Unregistering existing virtual disk"
		if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi
		VBoxManage closemedium \
		$XILINX_FILE_COND
		set +x
	else
		echo "Another Virtualbox disk with this name is already registered."
		echo "Name: " $XILINX_FILE_COND
		echo "Please close, remove or rename this virtual disk before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

echo " - Existing Virtual Disks (unregistered)..."
if [[ -f $XILINX_FILE_COND ]]; then
	# vm settings file already exists
	echo "   - Virtualbox Disk ["$XILINX_FILE_COND"] already exists!"
	if [[ $FLAG_REMOVE_EXISTING ]]; then
		# Remove existing settings file
		echo "   - Removing existing virtual disk file"
		if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi
		rm -f $XILINX_FILE_COND
		set +x
	else
		echo "A conflicting Virtualbox disk file was found."
		echo "Name: " $XILINX_FILE_COND
		echo "Please remove or rename this file before continuing."
		echo "*************************************"
		show_opts
		echo "*************************************"
		echo "ABORT: Terminating build script."
		exit $EX_OSFILE
	fi
fi

echo " - Guest Additions ISO ..."
if [[ -f $VIRTUALBOX_ADDITIONS_ISO ]]; then
	# Guest Additions ISO already exists
	echo "	- Virtualbox Guest Additions ISO ["$VIRTUALBOX_ADDITIONS_ISO"] exists."
else
	echo "Virtualbox Guest Additions ISO was NOT found."
	echo "Location Expected: "$VIRTUALBOX_ADDITIONS_ISO
	echo "ABORT: Terminating build script."
	exit $EX_OSFILE
fi

echo "-------------------------------------"

########################################################################################
# Create new resources
########################################################################################

# Create the virtual machine
echo ""
echo "--------------------------------"
echo " Creating the Virtual Machine..."
echo " - "$XILINX_VM_DISK_NAME
echo "--------------------------------"

if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage createvm \
--name $XILINX_VM_NAME \
--ostype Ubuntu_64 \
--basefolder $XILINX_BUILD_TMPDIR \
--register

set +x

# Verify machine was created successfully
if [[ "$(VBoxManage list vms | grep $XILINX_VM_NAME)" == "" ]]; then
	# vm doesn't exist, wasn't created
	echo "Failed to create ["$XILINX_VM_NAME"]"
	exit $EX_OSFILE
fi

# Create the virtual disk
echo ""
echo "--------------------------------"
echo " Creating the Virtual Disk..."
echo " $XILINX_VM_DISK_NAME"
echo "--------------------------------"

if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage createmedium \
--filename $XILINX_VM_PATH/$XILINX_VM_DISK_NAME \
--format VDI \
--variant Standard \
--size $XILINX_MAX_VM_SIZE_MB

set +x

# Verify disk was created successfully
if [[ "$(VBoxManage showmediuminfo $XILINX_VM_PATH/$XILINX_VM_DISK_NAME | grep $XILINX_VM_PATH/$XILINX_VM_DISK_NAME)" == "" ]]; then
	# vm disk doesn't exist, wasn't created
	echo "Failed to create [$XILINX_VM_PATH/$XILINX_VM_DISK_NAME]"
	exit $EX_OSFILE
fi

# Configure virtual machine resources
echo ""
echo "--------------------------------"
echo " Configuring machine resources.."

echo " - Adding SATA controller..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storagectl $XILINX_VM_NAME \
--name SATA \
--add sata \
--controller IntelAHCI

set +x

echo "   - Attaching virtual disk image"
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storageattach $XILINX_VM_NAME \
--storagectl SATA \
--port 0 \
--device 0 \
--type hdd \
--medium $XILINX_VM_PATH/$XILINX_VM_DISK_NAME

set +x

echo " - Adding IDE controller..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storagectl $XILINX_VM_NAME \
--name IDE \
--add ide \
--controller ICH6

set +x

echo "   - Attaching installer as optical disk"
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage storageattach $XILINX_VM_NAME \
--storagectl IDE \
--port 0 \
--device 0 \
--type dvddrive \
--medium $UBUNTU_INSTALLER

set +x

echo "--------------------------------"

# Configure virtual machine resources
echo ""
echo "--------------------------------"
echo " Configuring machine settings..."

echo " - Configuring virtual cpus..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--cpus 2

set +x

echo " - Configuring system RAM..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--memory 4096

set +x

echo " - Configuring video RAM..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--vram 16

set +x

echo " - Configuring extended IRQ support..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--ioapic on

set +x

echo " - Configuring device boot order..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--boot1 dvd \
--boot2 disk \
--boot3 none \
--boot4 none

set +x

echo " - Configuring audio..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--audio none

set +x

echo " - Configuring usb..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--usb on \
--usbehci on \
--usbxhci off

set +x

echo " - Configuring the graphics controller..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--graphicscontroller vmsvga

set +x

echo " - Configuring network adapters..."
if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage modifyvm $XILINX_VM_NAME \
--nic1 nat \
--nic2 none \
--nic3 none \
--nic4 none

set +x

echo "--------------------------------"

# Execute unattended install
echo ""
echo "--------------------------------"
echo " Launching unattended install..."

if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

VBoxManage unattended install \
$XILINX_VM_NAME \
--user=$USER_ACCT \
--password=$USER_ACCT \
--country=US \
--time-zone=EST \
--iso=$UBUNTU_INSTALLER \
--start-vm=gui \
--install-additions \
--additions-iso=$VIRTUALBOX_ADDITIONS_ISO \
--script-template=$UBUNTU_PRESEED_TEMPLATE \
--post-install-template=$UBUNTU_POSTINSTALL_TEMPLATE 
#--post-install-command=\
#'printf '#!/bin/bash -e\nVBoxControl guestproperty set installation_complete y\n" > /etc/rc.local'\
#' && chmod +x /etc/rc.local'
#--post-install-command=$POSTINSTALL_COMMAND 
#--post-install-command='ls -al /usr/bin && VBoxControl guestproperty set installation_complete y'
#--post-install-command='echo "Test Post Install Command" > ~/post-install.log'
#--post-install-command='VBoxManage guestproperty set $XILINX_VM_NAME installation_complete y'

set +x

# Wait for installation to complete
# Note sure where/if this is officially documented, but found
# https://renenyffenegger.ch/notes/Companies-Products/Oracle/VM-VirtualBox/command-line/PowerShell/unattended-os-installation

echo "--------------------------------"
echo " Waiting for unattended install completion..."

VBoxManage guestproperty wait $XILINX_VM_NAME installation_complete

echo "--------------------------------"
echo " Powering off machine..."

VBoxManage controlvm $XILINX_VM_NAME poweroff

# Grab End Time
XILINX_BUILD_END_TIME=`date`
# Docker Image Build Complete
echo "-----------------------------------"
echo "Task Complete..."
echo "STARTED :"$XILINX_BUILD_START_TIME
echo "ENDED   :"$XILINX_BUILD_END_TIME
echo "-----------------------------------"
