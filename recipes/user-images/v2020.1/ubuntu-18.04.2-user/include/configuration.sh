#!/bin/bash
########################################################################################
# Image Build Variable Custom Configuration:
#
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer, Embedded Software
#
# Created: 
#	- 8/5/2020
#
########################################################################################


########################################################################################
# Xilinx Build Script Debug Tracing
########################################################################################
#BUILD_DEBUG=1 Turns shell command expansion on in Xilinx build scripts
#BUILD_DEBUG=0 Turns shell expandion off in Xilinx build scripts
BUILD_DEBUG=0

########################################################################################
# Override Xilinxfile Build Arguments:
########################################################################################
# User account information
# USER_ACCT: user account name within Xilinx image
# HOME_DIR : user account home directory
USER_ACCT=xilinx
HOME_DIR=/home/$USER_ACCT

# GIT Configuration
# GIT_USER_NAME: Username for git configuration
# GIT_USER_EMAIL: email address for git configuration
GIT_USER_NAME="Xilinx User"
GIT_USER_EMAIL="Xilinx.User@dummyaddress.com"

# Xilinx base OS
XILINX_BASE_OS=ubuntu
XILINX_BASE_OS_VERSION=18.04.2

# Xilinx Release Information
XLNX_RELEASE_VERSION=v2020.1

# Xilinx Virtual Machine Max Disk Size
XILINX_MAX_VM_SIZE_MB=100000 # 100GB

# Location the build is executed from
XILINX_BUILD_WORKING_DIR=`pwd`
XILINX_BUILD_TMPDIR=$XILINX_BUILD_WORKING_DIR/_vm

# Xilinx build image parameters
XILINX_VM_VERSION=$XLNX_RELEASE_VERSION

# Xilinx Image Name
XILINX_VM_NAME=$XILINX_BASE_OS-$XILINX_BASE_OS_VERSION-user
XILINX_VM_PATH=$XILINX_BUILD_TMPDIR/$XILINX_VM_NAME
XILINX_VM_SETTINGS=$XILINX_VM_NAME.vbox
XILINX_VM_DISK_NAME=$XILINX_VM_NAME.vdi

########################################################################################
# Ubuntu Related Information
########################################################################################
# - Ubuntu 18.04.2 LTS Desktop ISO
#	- http://old-releases.ubuntu.com/releases/18.04.2/ubuntu-18.04-desktop-amd64.iso
#
# - Ubuntu 18.04.2 LTS Server ISO
#	- http://old-releases.ubuntu.com/releases/18.04.2/ubuntu-18.04-server-amd64.iso
#
#UBUNTU_INSTALL_IMAGE=ubuntu-18.04-2-desktop-amd64.iso
UBUNTU_INSTALL_IMAGE=ubuntu-$XILINX_BASE_OS_VERSION-server-amd64.iso
UBUNTU_SHA256_FILE=SHA256SUMS
UBUNTU_BASE_URL=http://old-releases.ubuntu.com/releases/18.04.2

INSTALL_CONFIGS_DIR=configs
INSTALL_DEPENDS_DIR=depends
XILINX_BUILD_DEPENDS_DIR=$XILINX_BUILD_WORKING_DIR/$INSTALL_DEPENDS_DIR
UBUNTU_INSTALLER=$XILINX_BUILD_DEPENDS_DIR/$UBUNTU_INSTALL_IMAGE

UBUNTU_PRESEED_FILE=ubuntu_preseed.cfg 
UBUNTU_PRESEED_TEMPLATE=$INSTALL_CONFIGS_DIR/$UBUNTU_PRESEED_FILE

UBUNTU_POSTINSTALL_FILE=debian_postinstall.sh
UBUNTU_POSTINSTALL_TEMPLATE=$INSTALL_CONFIGS_DIR/$UBUNTU_POSTINSTALL_FILE



# OLDER DEFINITIONS CAN BE REMOVED BELOW

########################################################################################
########################################################################################
# Target Path Variables for Xilinx Image:
########################################################################################
########################################################################################
# Xilinx Tool Install Location
XLNX_INSTALL_LOCATION=/opt/Xilinx

########################################################################################
########################################################################################
# Host OS Path Variables for Xilinx build:
########################################################################################
########################################################################################
XILINX_INSTALL_DIR=.

# Xilinx Configuration and Dependency folder definitions
# These locations must be within the Xilinx build context
# (meaning accessible from the base folder of the Xilinxfile)
# INSTALL_CONFIGS_DIR: folder in Xilinx context on host for
#	build related configuration files for headless install
# INSTALL_DEPENDS_DIR:  folder in Xilinx context on host for
#	build related installers/downloads used in installation


########################################################################################
########################################################################################
# Depdendency / Configuration File Related Variables
########################################################################################
########################################################################################
# Configuration Files for batch mode installation
# KEYBOARD_CONFIG_FILE:	Keyboard setting configuration file
#	for headless selection of the keyboard setup
#	Required for the X-based XSDK installer package
KEYBOARD_CONFIG_FILE=$INSTALL_CONFIGS_DIR/keyboard_settings.conf

# Configuration file for xterm sessions inside of the Xilinx container
# XTERM_CONFIG_FILE: XTerm session configuratino file
#	Changes color scheme and font to something more readable (default is white background)
#   Changes scrollback to 1 million lines and enables the scroll bar
#   Notes on Copy-Paste with the host:
#	 	Copy from Host to XTerm:
#			- Host may copy with "CTRL-C" or "Right-Click->Copy"
#			- Use center mouse button (scroll wheel) to paste into XTerm session
#
#		Copy from XTerm to Host:
#			- Select text in XTerm session to copy using cursor (left-click and drag over text to copy)
#			- Host may paste with center mouse button (scroll wheel)
#			- Host clipboard contents from host copy maintained in separate buffer (CTRL-V)
#-------------
XTERM_CONFIG_FILE=$INSTALL_CONFIGS_DIR/XTerm

# Configuration Files for minicom session inside of the Xilinx container
# MINICOM_CONFIG_FILE: Minicom default settings configuration file
#   115200-8-N-1, no hardware flow control by default
MINICOM_CONFIG_FILE=$INSTALL_CONFIGS_DIR/.minirc.dfl

# Local Python3 http server to transfer files into container
# INSTALL_SERVER_URL: Set automatically later in this script when
#	http server is spawned for file transfer to container
INSTALL_SERVER_URL=0.0.0.0:8000

########################################################################################
########################################################################################

# Define File system error code
EX_OSFILE=72