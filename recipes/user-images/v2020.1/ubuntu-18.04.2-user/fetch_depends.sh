#!/bin/bash
########################################################################################
# Fetch dependencies for image generation :
# - Ubuntu ISO Installer(s)
# - Ubuntu Base Root Filesystem(s)
#
# Maintainer:
#	- Jason Moss (jason.moss@avnet.com)
#	- Xilinx Applications Engineer, Embedded Software
#
# Created: 
#	- 8/10/2020
############################

# Get path definitions
source ../../../../includes/paths.sh

# Source ubuntu 18.04.2 definitions
source $path_include/ubuntu_18.04.2_defs.sh

# define options
function show_opts {
	echo "Syntax:"
	echo "-------"
	echo "${0} --<option>"
	echo ""
	echo "Valid Options:"
	echo "--------------"
	echo "  --debug"
	echo ""
	echo "		Enable debug output"
	echo ""
	echo "  --remove-existing"
	echo ""
	echo "      Remove existing files."
	echo ""
	echo "  --verify-checksums-only"
	echo ""
	echo "      Only verify checksums for existing files, skip download process"
	echo ""
	echo " --help"
	echo ""
	echo "      display this syntax help"
	echo ""
}

# Init command ling argument flags
BUILD_DEBUG=0 # Enable extra debug messages
FLAG_REMOVE_EXISTING=0 # Remove any existing files before attempting a download
FLAG_VERIFY_CHECKSUMS_ONLY=0 # Verify Checksums only

# Process Command line arguments
PARAMS=""

while (("$#")); do
	case "$1" in
		--debug) # Enable debug output
			BUILD_DEBUG=1
			echo "Set: BUILD_DEBUG=$BUILD_DEBUG"
			shift
			;;
		--remove-existing) # remove all existing files
			FLAG_REMOVE_EXISTING=1
			if [ $BUILD_DEBUG -ne 0 ]; then echo "Set: FLAG_REMOVE_EXISTING=$FLAG_REMOVE_EXISTING"; fi
			shift
			;;
		--verify-checksums-only) # verify checksums
			FLAG_VERIFY_CHECKSUMS_ONLY=1
			if [ $BUILD_DEBUG -ne 0 ]; then echo "Set: FLAG_VERIFY_CHECKSUMS_ONLY=$FLAG_VERIFY_CHECKSUMS_ONLY"; fi
			shift
			;;			
		--help) # display syntax
			show_opts
			exit 0
			;;
		-*|--*=) # unsupported flags
			echo "ERROR: Unsupported option $1" >&2
			show_opts
			exit 1
			;;
		*) # all other parameters pass through
			PARAMS="$PARAMS $1"
			shift
			;;
	esac
done

# reset positional arguments
eval set -- "$PARAMS"

# Set the download count
DOWNLOAD_COUNT=${#depends_url[@]}

if [ $BUILD_DEBUG -ne 0 ]; then 
	# Dump include variables used in this script
	echo "Base Path       : [$path_base]"
	echo "Include Path    : [$path_include]"
	echo "Downloads Path  : [$path_downloads]"
	echo "Total Downloads : [$DOWNLOAD_COUNT]"

	for (( i=0; i < $DOWNLOAD_COUNT; i++ )); do
	echo " Download[$i]   : --------------------"
	echo "   URL[$i]          : [${depends_url[$i]}]"
	echo "   Image[$i]        : [${depends_image[$i]}]"
	echo "   ChecksumType[$i] : [${depends_checksum_type[$i]}]"
	echo "   ChecksumFile[$i] : [${depends_checksum_file[$i]}]"
	done
fi

# Grab Start Time
FETCH_START_TIME=`date`

# Flow overview:
# --------------
# 1. Does download exist?
#    Y -> verify checksum?
#         Y -> done for this image
#         N -> download image
#    N -> download image
# 2. Download image
# 3. Verify checksum?
#    Y -> done
#    N -> ABORT
# --------------

#if [ $BUILD_DEBUG -ne 0 ]; then set -x; fi

# Check for existing downloads
for (( i=0; i < $DOWNLOAD_COUNT; i++ )); do
	if [[ FLAG_VERIFY_CHECKSUMS_ONLY -eq 0 ]]; then
				
		if [[ FLAG_REMOVE_EXISTING -ne 0 ]]; then
			if [ $BUILD_DEBUG -ne 0 ]; then echo "- [$i]: Removing existing files."; fi
				rm -f $path_downloads/${depends_image[$i]}
				rm -f $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]}
				rm -f $path_downloads/${depends_checksum_file[$i]}
		fi

		# Does the download already exist?
		if [[ -f $path_downloads/${depends_image[$i]} ]] || [[ -L $path_downloads/${depends_image[$i]} ]]; then
			if [ $BUILD_DEBUG -ne 0 ]; then echo "- [$i]: Skipping Download (file exists)."; fi
		else
			echo "- [$i]: Downloading [${depends_image[$i]}]"
			if [ $BUILD_DEBUG -ne 0 ]; then 
				wget -nv -x --show-progress \
					${depends_url[$i]}/${depends_image[$i]} \
					-O $path_downloads/${depends_image[$i]}
			else
				wget -q -x \
					${depends_url[$i]}/${depends_image[$i]} \
					-O $path_downloads/${depends_image[$i]}
			fi
		fi

		# Does the download have a checksum file?
		if [[ -f $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]} ]]; then
			if [ $BUILD_DEBUG -ne 0 ]; then echo "- [$i]: Skipping Download (checksum file exists)."; fi
		else
			echo "- [$i]: Downloading [${depends_image[$i]}:${depends_checksum_file[$i]}]"
			if [ $BUILD_DEBUG -ne 0 ]; then 
				wget -nv -x --show-progress \
					${depends_url[$i]}/${depends_checksum_file[$i]} \
					-O $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]}
			else
				wget -q -x \
					${depends_url[$i]}/${depends_checksum_file[$i]} \
					-O $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]}
			fi
			
			# Separate out checksum information for this particular image and overwrite download
			cat $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]} \
				| grep ${depends_image[$i]} \
				> $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]}.tmp \
				&& \
				mv $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]}.tmp \
				$path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]}
			
			if [ $BUILD_DEBUG -ne 0 ]; then 
				echo "- [$i]: Downloaded Checksum"
				cat $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]}
				echo "---------------------------"
			fi
		fi

	fi
	
	# Validate checksum
	if [[ -f $path_downloads/${depends_image[$i]} ]] || [[ -L $path_downloads/${depends_image[$i]} ]]; then
		if [[ -f $path_downloads/${depends_image[$i]}.${depends_checksum_type[$i]} ]]; then
			# validate checksum
			pushd $path_downloads > /dev/null 2>&1
			echo "- [$i]: Validate Checksum"
			if [ $BUILD_DEBUG -ne 0 ]; then 
				sha256sum -c --quiet ${depends_image[$i]}.${depends_checksum_type[$i]}
			else
				sha256sum -c ${depends_image[$i]}.${depends_checksum_type[$i]}
			fi

			if [[ $? -ne 0 ]]; then
				echo "********************************************"
				echo "** Checksum FAILED for [${depends_image[$i]}]"
				echo "** Aborting Script"
				echo "********************************************"
				popd
				exit $EX_OSFILE
			fi
		else
			# checksum missing
			echo "********************************************"
			echo "** Checksum MISSING for [${depends_image[$i]}]"
			echo "** Aborting Script"
			echo "********************************************"
			popd
			exit $EX_OSFILE
		fi
	else
		# download missing
		# checksum missing
		echo "********************************************"
		echo "** File MISSING for [${depends_image[$i]}]"
		echo "** Aborting Script"
		echo "********************************************"
		popd
		exit $EX_OSFILE
	fi
done

# List images and checksum files
if [ $BUILD_DEBUG -ne 0 ]; then 
	# Dump include variables used in this script
	echo "Base Path       : [$path_base]"
	echo "Include Path    : [$path_include]"
	echo "Downloads Path  : [$path_downloads]"
	echo "Total Downloads : [$DOWNLOAD_COUNT]"

	echo "----------------------------------------------"
	for (( i=0; i < $DOWNLOAD_COUNT; i++ )); do
		ls -al $path_downloads/${depends_image[$i]}*
	done
fi

# Grab End Time
FETCH_END_TIME=`date`
# Image Downloads Complete
echo "-----------------------------------"
echo "Task Complete..."
echo "STARTED :"$FETCH_START_TIME
echo "ENDED   :"$FETCH_END_TIME
echo "-----------------------------------"

